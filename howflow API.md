## 工作流相关接口

### 获取工作流列表

- url：/api/v1/service/workflows/

- method：get

- 使用场景

获取到工作流列表后，用户选择对应的工作流来新建对应的工单。如果需要多级类型，可以在调用方系统保存对应关系。 如调用方的“权限申请-VPN权限申请“对应loonflow中id为1的workflow,调用方的“权限申请-服务器权限申请“对应loonflow中id为2的workflow

- 请求参数

| 参数名   | 类型    | 必填 | 说明                          |
| -------- | ------- | ---- | ----------------------------- |
| page     | int     | 否   | 页码，默认1                   |
| per_page | int     | 否   | 每页个数，默认10              |
| name     | varchar | 否   | 支持根据workflow name模糊查询 |

- 返回数据

```json
{
  "code": 0,
  "data": {
    "total": 2,
    "page": 1,
    "per_page": 10,
    "value": [{
      "name": "请假申请",
      "creator": "admin",
      "description": "请假申请",
      "gmt_created": "2018-04-23 20:49:32"
    }, {
      "name": "vpn申请",
      "creator": "admin",
      "description": "vpn权限申请",
      "gmt_created": "2018-05-06 12:32:36"
    }]
  },
  "msg": ""
}
```

### 获取工作流初始状态

- url: api/v1/service/init_state/`workflow_id`

- method: get

- 请求参数

无

- 使用场景

用于获取创建工单时对应工作流的初始状态信息，返回内容包括创建工单时需要填写的表单内容，可以执行的提交操作

- 返回数据

```json
{
  "msg": "",
  "code": 0,
  "data": {
    "order_id": 0,
    "workflow_id": 1,
    "name": "新建中",
    "participant_type_id": 1,
    "distribute_type_id": 1,
    "participant": "wangfei",
    "is_hidden": false,
    "type_id": 1,
    "gmt_created": "2018-04-23 20:53:33",
    "id": 1,
    "transition": [{
      "transition_id": 1,
      "transition_name": "提交"
    }, {
      "transition_id": 2,
      "transition_name": "保存"
    }],
    "sub_workflow_id": 0,
    "creator": "admin",
    "label": {},
    "field_list": [{
      "order_id": 20,
      "field_key": "title",
      "field_attribute": 2,
      "value": null,
      "name": "标题",
      "field_type_id": 5
    }, {
      "order_id": 35,
      "field_key": "leave_proxy",
      "field_attribute": 2,
      "field_type_id": 60,
      "field_value": null,
      "field_name": "代理人",
      "field_choice": {}
    }, {
      "order_id": 25,
      "field_key": "leave_end",
      "field_attribute": 2,
      "field_type_id": 30,
      "field_value": null,
      "field_name": "结束时间",
      "field_choice": {}
    }, {
      "order_id": 20,
      "field_key": "leave_start",
      "field_attribute": 2,
      "field_type_id": 30,
      "field_value": null,
      "field_name": "开始时间",
      "field_choice": {}
    }, {
      "order_id": 40,
      "field_key": "leave_type",
      "field_attribute": 2,
      "field_type_id": 40,
      "field_value": null,
      "field_name": "请假类型",
      "field_choice": {
        "1": "年假",
        "2": "调休",
        "3": "病假",
        "4": "婚假"
      }
    }, {
      "order_id": 45,
      "field_key": "leave_reason",
      "field_attribute": 2,
      "field_type_id": 55,
      "field_value": null,
      "field_name": "请假原因及相关附件",
      "field_choice": {}
    }, {
      "order_id": 30,
      "field_key": "leave_days",
      "field_attribute": 2,
      "field_type_id": 5,
      "field_value": null,
      "field_name": "请假天数(0.5的倍数)",
      "field_choice": {}
    }]
  }
}
```

### 获取工作流状态列表

- url: api/v1/service/states
- method：get
- 使用场景

可用于用户查询工单列表时选择工作流类型后，显示该工作流类型拥有的状态，然后可以再根据工单当前状态来查询。 另外可用于管理员干预工单强制修改状态时 允许选择的目标状态

- 返回数据

  ```json
  {
    "code": 0,
    "data": {
      "value": [{
        "id": 1,
        "creator": "admin",
        "gmt_created": "2018-04-23 20:53:33",
        "gmt_modified": "2018-05-13 11:42:11",
        "is_deleted": false,
        "name": "\u65b0\u5efa\u4e2d",
        "workflow_id": 1,
        "sub_workflow_id": 0,
        "is_hidden": false,
        "order_id": 0,
        "type_id": 1,
        "remember_last_man_enable": false,
        "participant_type_id": 1,
        "participant": "wangfei",
        "distribute_type_id": 1,
        "state_field_str": {
          "title": 2,
          "leave_start": 2,
          "leave_end": 2,
          "leave_days": 2,
          "leave_proxy": 2,
          "leave_type": 2,
          "leave_reason": 2
        },
        "label": {},
        "participant_info": {
          "participant": "wangfei",
          "participant_name": "wangfei",
          "participant_type_id": 1,
          "participant_type_name": "\u4e2a\u4eba",
          "participant_alias": "wangfei"
        }
      },{
        "id": 2,
        "creator": "admin",
        "gmt_created": "2018-04-30 15:51:41",
        "gmt_modified": "2018-05-11 06:52:39",
        "is_deleted": false,
        "name": "\u7ed3\u675f",
        "workflow_id": 1,
        "sub_workflow_id": 0,
        "is_hidden": false,
        "order_id": 6,
        "type_id": 2,
        "remember_last_man_enable": false,
        "participant_type_id": 0,
        "participant": "",
        "distribute_type_id": 1,
        "state_field_str": {},
        "label": {},
        "participant_info": {
          "participant": "",
          "participant_name": "",
          "participant_type_id": 0,
          "participant_type_name": "",
          "participant_alias": ""
        }
      }],
      "per_page": 10,
      "page": 1,
      "total": 5
    },
    "msg": ""
  }
  ```



## 工单相关接口

### 获取工单列表

- url: /api/v1/service/tickets/

- method：get

- 请求参数

| 参数名       | 类型    | 必填 | 说明                                                         |
| ------------ | ------- | ---- | ------------------------------------------------------------ |
| sn           | varchar | 否   | 流水号，支持根据sn的前几位模糊查询                           |
| title        | varchar | 否   | 工单标题，模糊查询                                           |
| create_start | varchar | 否   | 创建时间起,如果创建时间起 和创建时间止 都不提供，则只返回最近一年的数据 |
| create_end   | varchar | 否   | 创建时间止,如果创建时间起 和创建时间止 都不提供，则只返回最近一年的数据 |
| workflow_ids | varchar | 否   | 工作流ids，逗号隔开多个工作流id, 如"1,2,3"                   |
| state_ids    | varchar | 否   | 状态ids,逗号隔开多个状态id,如"1,2,3"                         |
| ticket_ids   | varchar | 否   | 工单ids, 逗号隔开多个id，如"1,2,3"                           |
| reverse      | varchar | 否   | 是否按照创建时间倒序，"0"或者"1"，默认倒序                   |
| page         | int     | 否   | 页码，默认1                                                  |
| per_page     | varchar | 否   | 每页个数，默认10                                             |
| act_state_id | int     | 否   | 工单的进行状态, 0草稿中(也就是初始状态)、1进行中 2被退回 3被撤回 4完成 |
| category     | varchar | 是   | 类型('all':所有工单, 'owner':我创建的工单, 'duty':我的待处理工单, 'relation':我的关联工单[包括我新建的、我处理过的、曾经需要我处理过的工单。注意这里只考虑历史状态，工单将来状态的处理人不考虑]|
| cc           | varchar | 否   | 只有在抄送的时候才传次参数，并且category=all, cc类型('all':所有我抄送的,'unread': 我的未读抄送)|

- 返回数据

```json
{
  "msg": "",
  "code": 0,
  "data": {
    "value": [{
      "participant_info": {
        "participant_type_id": 1,
        "participant": "1",
        "participant_name": "zhangsan",
        "participant_type_name": "个人",
        "participant_alias": "张三"
      },
      "gmt_created": "2018-05-15 07:16:38",
      "parent_ticket_state_id": 0,
      "state": {
        "state_name": "发起人-确认中",
        "state_id": 10
      },
      "creator": "lilei",
      "parent_ticket_id": 0,
      "title": "vpn申请",
      "gmt_modified": "2018-05-22 07:26:54",
      "workflow": {
        "workflow_name": "vpn申请",
        "workflow_id": 2
      },
      "sn": "loonflow_201805150001",
      "id": 17
    }],
    "total": 1,
    "page": 1,
    "per_page": 10
  }
}
```

### 新建工单

- url： /api/v1/service/create_ticket/

- method：post

- 请求参数

| 参数名                 | 类型    | 必填 | 说明                                                         |
| ---------------------- | ------- | ---- | ------------------------------------------------------------ |
| workflow_id            | int     | 是   | 工作流id(工单关联的工作流的id)                               |
| transition_id          | int     | 是   | 新建工单时候的流转id（通过workflow/{id}/init_state接口可以获取新建工单时允许的transition） |
| parent_ticket_id       | int     | 否   | 父工单的id(用于子工单的逻辑，如果新建的工单是某个工单的子工单需要填写父工单的id) |
| parent_ticket_state_id | int     | 否   | 父工单的状态（子工单是和父工单的某个状态关联的），如果提供parent_ticket_id，则此参数也必须 |
| suggestion             | varchar | 否   | 处理意见（与处理工单类型，用户在处理工单的时候点击了按钮操作 可以填写附加的一些意见如:麻烦尽快处理） |
| 其他必填字段           | N/A     | 否   | 其他必填字段或可选字段（在配置工作流过程中,会配置工作流初始状态必填和可选的字段。在新建工单时候必须提供必填字段。如请假申请工单，配置了自定义字段请假天数days，工单初始状态也设置了days为必填，那么新建此类工单时候就必选提供days) |

- 返回数据

```json
{
  "msg": "",
  "code": 0,
  "data": {
    "ticket_id": 1
  }
}
```

### 获取工单详情

- url：/api/v1/service/tickets/`ticket_id`

- method: get

- 请求参数

无

- 返回数据

```json
{
  "code": 0,
  "msg": "",
  "data": {
    "value": {
      "workflow_id": 2,
      "in_add_node": true,
      "gmt_created": "2018-05-15 07:16:38",
      "id": 17,
      "relation": "guiji,wangwu,lilei",
      "title": "vpn\u7533\u8bf72",
      "sn": "loonflow_201805150001",
      "parent_ticket_id": 0,
      "creator": "lilei",
      "script_run_last_result": true,
      "gmt_modified": "2018-05-22 07:26:54",
      "act_state_id": 1,
      "multi_all_person": "{}",
      "creator_info": {
        "email": "lilei@163.com",
        "alias": "\u674e\u78ca",
        "dept_info": {
          "creator_info": {
            "creator_id": 1,
            "creator_alias": "\u8d85\u7ea7\u7ba1\u7406\u5458"
          },
          "leader": "lilei",
          "parent_dept_info": {
            "parent_dept_name": "\u603b\u90e8",
            "parent_dept_id": 1
          },
          "approver_info": [],
          "parent_dept_id": 1,
          "name": "\u6280\u672f\u90e8",
          "is_deleted": false,
          "creator": "admin",
          "gmt_modified": "2018-05-09 06:45:27",
          "label": "",
          "id": 2,
          "approver": "",
          "gmt_created": "2018-04-14 23:37:06",
          "leader_info": {
            "leader_alias": "\u674e\u78ca",
            "leader_username": "lilei"
          }
        },
        "username": "lilei",
        "phone": "13888888888",
        "is_active": true
      },
      "participant_type_id": 3,
      "state_id": 10,
      "is_end": false,
      "is_deleted": false,
      "field_list": [{
        "field_value": "loonflow_201805150001",
        "label": {},
        "boolean_field_display": {},
        "field_type_id": 5,
        "field_template": "",
        "field_choice": {},
        "field_key": "sn",
        "field_attribute": 1,
        "description": "\u5de5\u5355\u7684\u6d41\u6c34\u53f7",
        "default_value": null,
        "order_id": 10,
        "field_name": "\u6d41\u6c34\u53f7"
      }, {
        "field_value": "\u53d1\u8d77\u4eba-\u786e\u8ba4\u4e2d",
        "label": {},
        "boolean_field_display": {},
        "field_type_id": 5,
        "field_template": "",
        "field_choice": {},
        "field_key": "state.state_name",
        "field_attribute": 1,
        "description": "\u5de5\u5355\u5f53\u524d\u72b6\u6001\u7684\u540d\u79f0",
        "default_value": null,
        "order_id": 41,
        "field_name": "\u72b6\u6001\u540d"
      }, {
        "field_value": "\u603b\u90e8",
        "label": {},
        "boolean_field_display": {},
        "field_type_id": 5,
        "field_template": "",
        "field_choice": {},
        "field_key": "participant_info.participant_name",
        "field_attribute": 1,
        "description": "\u5de5\u5355\u7684\u5f53\u524d\u5904\u7406\u4eba",
        "default_value": null,
        "order_id": 50,
        "field_name": "\u5f53\u524d\u5904\u7406\u4eba"
      }, {
        "field_value": "vpn\u7533\u8bf7",
        "label": {},
        "boolean_field_display": {},
        "field_type_id": 5,
        "field_template": "",
        "field_choice": {},
        "field_key": "workflow.workflow_name",
        "field_attribute": 1,
        "description": "\u5de5\u5355\u6240\u5c5e\u5de5\u4f5c\u6d41\u7684\u540d\u79f0",
        "default_value": null,
        "order_id": 60,
        "field_name": "\u5de5\u4f5c\u6d41\u540d\u79f0"
      }],
      "parent_ticket_state_id": 0,
      "add_node_man": "zhangsan",
      "participant": "1",
      "state_info": {
        "id": 10,
        "creator": "admin",
        "gmt_created": "2018-04-30 15:47:58",
        "gmt_modified": "2018-05-13 11:42:59",
        "is_deleted": false,
        "name": "\u4eba\u4e8b\u90e8\u95e8-\u5904\u7406\u4e2d",
        "workflow_id": 1,
        "is_hidden": false,
        "order_id": 4,
        "type_id": 0,
        "enable_retreat": false,
        "remember_last_man_enable": false,
        "participant_type_id": 1,
        "participant": "admin",
        "distribute_type_id": 1,
        "state_field_str": {
          "sn": 1,
          "title": 1,
          "leave_start": 1,
          "leave_end": 1,
          "leave_days": 1,
          "leave_proxy": 1,
          "leave_type": 1,
          "creator": 1,
          "gmt_created": 1,
          "leave_reason": 1
        },
        "label": {}
      }
    }
  }
}
```

### 工单修改

- url：/api/v1/service/tickets/`ticket_id`

- method: put/patch

- 请求参数

| 参数名                 | 类型    | 必填 | 说明                                                         |
| ---------------------- | ------- | ---- | ------------------------------------------------------------ |
| workflow_id            | int     | 是   | 工作流id(工单关联的工作流的id)                               |
| transition_id          | int     | 是   | 新建工单时候的流转id（通过workflow/{id}/init_state接口可以获取新建工单时允许的transition） |
| parent_ticket_id       | int     | 否   | 父工单的id(用于子工单的逻辑，如果新建的工单是某个工单的子工单需要填写父工单的id) |
| parent_ticket_state_id | int     | 否   | 父工单的状态（子工单是和父工单的某个状态关联的），如果提供parent_ticket_id，则此参数也必须 |
| suggestion             | varchar | 否   | 处理意见（与处理工单类型，用户在处理工单的时候点击了按钮操作 可以填写附加的一些意见如:麻烦尽快处理） |
| 其他必填字段           | N/A     | 否   | 其他必填字段或可选字段（在配置工作流过程中,会配置工作流初始状态必填和可选的字段。在新建工单时候必须提供必填字段。如请假申请工单，配置了自定义字段请假天数days，工单初始状态也设置了days为必填，那么新建此类工单时候就必选提供days) |

- 返回数据

```json
{
  "code": 0,
  "msg": "",
  "data": {
    "value": {
      "workflow_id": 2,
      "in_add_node": true,
      "gmt_created": "2018-05-15 07:16:38",
      "id": 17,
      "relation": "guiji,wangwu,lilei",
      "title": "vpn\u7533\u8bf72",
      "sn": "loonflow_201805150001",
      "parent_ticket_id": 0,
      "creator": "lilei",
      "script_run_last_result": true,
      "gmt_modified": "2018-05-22 07:26:54",
      "act_state_id": 1,
      "multi_all_person": "{}",
      "creator_info": {
        "email": "lilei@163.com",
        "alias": "\u674e\u78ca",
        "dept_info": {
          "creator_info": {
            "creator_id": 1,
            "creator_alias": "\u8d85\u7ea7\u7ba1\u7406\u5458"
          },
          "leader": "lilei",
          "parent_dept_info": {
            "parent_dept_name": "\u603b\u90e8",
            "parent_dept_id": 1
          },
          "approver_info": [],
          "parent_dept_id": 1,
          "name": "\u6280\u672f\u90e8",
          "is_deleted": false,
          "creator": "admin",
          "gmt_modified": "2018-05-09 06:45:27",
          "label": "",
          "id": 2,
          "approver": "",
          "gmt_created": "2018-04-14 23:37:06",
          "leader_info": {
            "leader_alias": "\u674e\u78ca",
            "leader_username": "lilei"
          }
        },
        "username": "lilei",
        "phone": "13888888888",
        "is_active": true
      },
      "participant_type_id": 3,
      "state_id": 10,
      "is_end": false,
      "is_deleted": false,
      "field_list": [{
        "field_value": "loonflow_201805150001",
        "label": {},
        "boolean_field_display": {},
        "field_type_id": 5,
        "field_template": "",
        "field_choice": {},
        "field_key": "sn",
        "field_attribute": 1,
        "description": "\u5de5\u5355\u7684\u6d41\u6c34\u53f7",
        "default_value": null,
        "order_id": 10,
        "field_name": "\u6d41\u6c34\u53f7"
      }, {
        "field_value": "\u53d1\u8d77\u4eba-\u786e\u8ba4\u4e2d",
        "label": {},
        "boolean_field_display": {},
        "field_type_id": 5,
        "field_template": "",
        "field_choice": {},
        "field_key": "state.state_name",
        "field_attribute": 1,
        "description": "\u5de5\u5355\u5f53\u524d\u72b6\u6001\u7684\u540d\u79f0",
        "default_value": null,
        "order_id": 41,
        "field_name": "\u72b6\u6001\u540d"
      }, {
        "field_value": "\u603b\u90e8",
        "label": {},
        "boolean_field_display": {},
        "field_type_id": 5,
        "field_template": "",
        "field_choice": {},
        "field_key": "participant_info.participant_name",
        "field_attribute": 1,
        "description": "\u5de5\u5355\u7684\u5f53\u524d\u5904\u7406\u4eba",
        "default_value": null,
        "order_id": 50,
        "field_name": "\u5f53\u524d\u5904\u7406\u4eba"
      }, {
        "field_value": "vpn\u7533\u8bf7",
        "label": {},
        "boolean_field_display": {},
        "field_type_id": 5,
        "field_template": "",
        "field_choice": {},
        "field_key": "workflow.workflow_name",
        "field_attribute": 1,
        "description": "\u5de5\u5355\u6240\u5c5e\u5de5\u4f5c\u6d41\u7684\u540d\u79f0",
        "default_value": null,
        "order_id": 60,
        "field_name": "\u5de5\u4f5c\u6d41\u540d\u79f0"
      }],
      "parent_ticket_state_id": 0,
      "add_node_man": "zhangsan",
      "participant": "1",
      "state_info": {
        "id": 10,
        "creator": "admin",
        "gmt_created": "2018-04-30 15:47:58",
        "gmt_modified": "2018-05-13 11:42:59",
        "is_deleted": false,
        "name": "\u4eba\u4e8b\u90e8\u95e8-\u5904\u7406\u4e2d",
        "workflow_id": 1,
        "is_hidden": false,
        "order_id": 4,
        "type_id": 0,
        "enable_retreat": false,
        "remember_last_man_enable": false,
        "participant_type_id": 1,
        "participant": "admin",
        "distribute_type_id": 1,
        "state_field_str": {
          "sn": 1,
          "title": 1,
          "leave_start": 1,
          "leave_end": 1,
          "leave_days": 1,
          "leave_proxy": 1,
          "leave_type": 1,
          "creator": 1,
          "gmt_created": 1,
          "leave_reason": 1
        },
        "label": {}
      }
    }
  }
}
```

### 获取工单可以做的操作

- url: api/v1/service/actions/`id`

- method：get

- 请求参数

无

- 返回数据

```json
{
  "msg": "",
  "data": {
    "value": [
      {
        "transition_name": "提交",
        "field_require_check": true,  # 默认为ture,如果此为否时， 不校验表单必填内容
        "transition_id": 1,
        "is_accept": false, # 不是接单,
        "in_add_node": false, # 不处于加签状态下
        "enable_alert": false,  # 是否弹窗告警，可用于当用户点击此操作的时确定是否弹窗信息
        "alert_text": "" # 弹窗中的消息内容
      },
      {
        "transition_name": "保存",
        "field_require_check": true,  # 默认为ture,如果此为否时， 不校验表单必填内容
        "transition_id": 2,
        "is_accept": false, # 不是接单,
        "in_add_node": false, # 不处于加签状态下
        "enable_alert": false,  # 是否弹窗告警，可用于当用户点击此操作的时确定是否弹窗信息
        "alert_text": "" # 弹窗中的消息内容
      }
    ]
    },
  "code": 0
}
```

如果当前处理人超过一个人(处理人类型为多人，部门、角色都有可能实际为多个人)，且当前状态的分配方式为主动接单，则会要求先接单,返回数据如下。 处理时需要处理人先接单(点击接单按钮时 调用接单接口).

```json
{
  "msg": "",
  "code": 0,
  "data": {
    "value": [
      {
        "transition_id": 0,
        "transition_name": "接单",
        "is_accept": true,  # 接单,
        "in_add_node": false,
        "field_require_check": false
      }
    ]
  }
}
```

当工单当前处于加签状态下，返回格式如下。 则用户点击“完成”按钮时，需要调用完成加签操作接口

```json
{
  "msg": "",
  "code": 0,
  "data": {
    "value": [
      {
        "transition_id": 0,
        "transition_name": "完成",
        "is_accept": false,
        "in_add_node": true, # 处于加签状态
        "field_require_check": false
      }
    ]
  }
}
```

### 接单

- url：api/v1/service/tickets/`id`/accept

- method：post

- 请求参数 : 无

- 使用场景

使用接口获取工单当前可以做的的操作后，如果data.value.is_accept==true,则需要用户先接单才能处理，即页面显示接单按钮， 用户点击后调用接单接口，将工单的当前处理人设置该用户

- 返回数据

```json
{
    "code": 0,
    "data": {},
    "msg": ""
}
```

### 转交

- url：api/v1/service/tickets/`id`/deliver

- method：post

- 请求参数

| 参数名          | 类型    | 必填 | 说明                                                         |
| --------------- | ------- | ---- | ------------------------------------------------------------ |
| target_username | varchar | 是   | 转交对象的用户名                                             |
| suggestion      | varchar | 否   | 转交意见                                                     |
| from_admin      | boolß   | 否   | 是否管理员强制转交，此参数用于对应工作流管理员或者超级管理员强制转交工单，传了from_admin,loonflow会校验用户是否是超级管理员或者该工作流的管理员 |

- 使用场景

在工单处理界面可以显示一个按钮“转交”，当用户认为当前工单自己处理不了时，可以将工单转交给合适的人处理。 另外作为管理员可以强制(即非工单当前处理人的情况下)将工单转交给别人ß

- 返回数据

```json
{
  "data": true,
  "code": 0,
  "msg": ""
}
```

### 加签

- url：api/v1/service/tickets/`id`/add_node

- method：post

- 请求参数

| 参数名          | 类型    | 必填 | 说明             |
| --------------- | ------- | ---- | ---------------- |
| target_username | varchar | 是   | 加签对象的用户名 |
| suggestion      | varchar | 否   | 加签意见         |

- 使用场景

当用户A提交了一个权限申请工单，达到运维人员处理人中状态，作为运维人员的B在处理过程中发现需要C先处理或者提供一些必要的信息，B才能处理。 那么B在处理工单界面可以点击”加签“按钮，弹窗中选择C。 系统调用loonflow的加签接口将工单加签给C。C处理完后点击”完成“按钮， 系统调用loonflow的加签完成接口， 工单处理人将回到B. 那么B就可以按照之前既定流程正常流转下去

- 返回数据

```json
{
  "data": {},
  "code": 0,
  "msg": ""
}
```

### 加签处理完成

- url：api/v1/service/tickets/`id`/add_node_end

- method：post

- 请求参数

| 参数名     | 类型    | 必填 | 说明         |
| ---------- | ------- | ---- | ------------ |
| suggestion | varchar | 否   | 加签完成意见 |

- 使用场景

使用场景 当A将工单加签给B.B在处理工单时候，界面将只显示“完成“按钮，点击后后端调用此接口，将工单基础表中的is_add_node设置为false

- 返回数据

```json
{
  "data": {},
  "code": 0,
  "msg": ""
}
```

### 新增工单评论/注释

- url：api/v1/service/comments

- method：post

- 请求参数

| 参数名     | 类型    | 必填 | 说明                                                         |
| ---------- | ------- | ---- | ------------------------------------------------------------ |
| suggestion | varchar | 是   | 处理意见（与处理工单类型，用户在处理工单的时候点击了按钮操作 可以填写附加的一些意见如:麻烦尽快处理） |
| ticket_id  | int     | 是   | 工单id                                                       |

- 返回数据

```json
{
  "code": 0,
  "msg": "",
  "data": {}
}
```

### 强制关闭工单

- url：api/v1/service/tickets/`id`/close

- method：post

- 请求参数

| 参数名     | 类型    | 必填 | 说明     |
| ---------- | ------- | ---- | -------- |
| suggestion | varchar | 否   | 关闭原因 |

- 使用场景

超级管理员在查看工单详情时，可以在界面上显示一个强制关闭工单的按钮，点击后调用关闭工单按钮，实现强制关闭工单。 另外工单创建人在工单处于初始状态下(创建人撤回、退回到初始状态等情况工单状态会处于初始状态)也可以强制关闭工单。

- 返回数据

```json
{
  "code": 0,
  "msg": "",
  "data": {}
}
```

### 撤回工单

- url：api/v1/service/tickets/`id`/retreat

- method：post

- 请求参数

| 参数名     | 类型    | 必填 | 说明     |
| ---------- | ------- | ---- | -------- |
| suggestion | varchar | 否   | 撤回原因 |

- 使用场景

在配置工作流状态时，可以指定某些状态下允许创建人撤回工单，那么当工单处于这些状态时，创建人可以撤回该工单(调用方前端在这个情况下显示一个撤回按钮)

- 返回数据

```json
{
  "code": 0,
  "msg": "",
  "data": {}
}
```

### 打印工单

- url：api/v1/service/tickets/to_print/

- method：post

- 请求参数

| 参数名     | 类型    | 必填 | 说明     |
| ---------- | ------- | ---- | -------- |
| media_id | varchar | 是   | 钉钉上传媒体文件 返回media_id |

- 使用场景

获取access_token请求 get  url  api/get_access_token/
钉钉上传文件请求 post url https://oapi.dingtalk.com/media/upload?access_token=08416c1c4ad03e918351a30791ba15ec&type=file
参数 media 文件对象 form-data
上传成功返回media_id

- 返回数据

```json
{
  "msg": "ok",
}
```

### 获取工单流转记录

- url：api/v1/service/logs/`id`

- method：get

- 请求参数

| 参数名      | 类型 | 必填 | 说明                                         |
| ----------- | ---- | ---- | -------------------------------------------- |
| ticket_data | int  | 否   | 是否返回每个操作时工单的所有字段信息，默认否 |

- 返回数据（ticket_data未传或ticket_data传0）

```json
{
  "msg": "",
  "data": {
    "total": 4,
    "value": [
      {
        "state": {
          "state_name": "发起人-确认中",
          "state_id": 5
        },
        "transition": {
          "transition_name": "确认完成",
          "transition_id": 5,
          "attribute_type_id": 3
        },
        "ticket_id": 1,
        "participant_info": {
          "participant_email": "lilei@163.com",
          "participant_alias": "李磊",
          "participant_phone": "13888888888",
          "participant": "lilei",
          "participant_type_id": 1
        },
        "gmt_modified": "2018-04-30 15:57:26",
        "gmt_created": "2018-04-30 15:56:02",
        "suggestion": "已经生效，感谢"
      },
      {
      "state": {
        "state_name": "技术人员-处理中",
        "state_id": 4
        },
      "transition": {
        "transition_name": "处理完成",
        "transition_id": 4
      },
      "ticket_id": 1,
      "participant_info": {
          "participant_email": "lilei@163.com",
          "participant_alias": "李磊",
          "participant_phone": "13888888888",
          "participant": "lilei",
          "participant_type_id": 1
        },
      "gmt_modified": "2018-04-30 15:57:14",
      "gmt_created": "2018-04-30 15:55:32",
      "suggestion": "处理完成"
      },
      {
      "state": {
        "state_name": "TL审批中",
        "state_id": 3
      },
      "transition": {
        "transition_name": "同意",
        "transition_id": 3
      },
      "ticket_id": 1,
      "participant_info": {
          "participant_email": "lilei@163.com",
          "participant_alias": "李磊",
          "participant_phone": "13888888888",
          "participant": "lilei",
          "participant_type_id": 1
        },
      "gmt_modified": "2018-04-30 15:57:00",
      "gmt_created": "2018-04-30 15:53:19",
      "suggestion": "同意处理"
      },
      {
      "state": {
        "state_name": "新建中",
        "state_id": 1
      },
      "transition": {
        "transition_name": "提交",
        "transition_id": 1
      },
      "ticket_id": 1,
      "gmt_modified": "2018-04-30 15:52:35",
      "gmt_created": "2018-04-10 17:39:33",
      "suggestion": "请尽快处理，谢谢"
      }],
    "page": 1,
    "per_page": 10
    },
  "code": 0
}
```

- 返回数据（ticket_data传1）

```json
{
      "msg": "",
      "data": {
              "total": 4,
              "value": [{
                              "state": {
                                      "state_name": "发起人-确认中",
                                      "state_id": 5
                              },
                              "transition": {
                                      "transition_name": "确认完成",
                                      "transition_id": 5,
                                      "attribute_type_id": 3
                              },
                              "ticket_id": 1,
                              "participant_info": {
                                      "participant_email": "lilei@163.com",
                                      "participant_alias": "李磊",
                                      "participant_phone": "13888888888",
                                      "participant": "lilei",
                                      "participant_type_id": 1
                              },
                              "gmt_modified": "2018-04-30 15:57:26",
                              "gmt_created": "2018-04-30 15:56:02",
                              "suggestion": "已经生效，感谢",
                              "ticket_data": {
                                      "title": "xxx",
                                      "sn": "xxxxx",
                                      "state_id": 1,
                                      "ticket_id": 1,
                                      "gmt_modified": "2018-04-30 15:57:26",
                                      "gmt_created": "2018-04-30 15:56:02",
                                      "xxxx": "....."
                              }
                      },
                      {
                              "state": {
                                      "state_name": "技术人员-处理中",
                                      "state_id": 4
                              },
                              "transition": {
                                      "transition_name": "处理完成",
                                      "transition_id": 4
                              },
                              "ticket_id": 1,
                              "participant_info": {
                                      "participant_email": "lilei@163.com",
                                      "participant_alias": "李磊",
                                      "participant_phone": "13888888888",
                                      "participant": "lilei",
                                      "participant_type_id": 1
                              },
                              "gmt_modified": "2018-04-30 15:57:14",
                              "gmt_created": "2018-04-30 15:55:32",
                              "suggestion": "处理完成",
                              "ticket_data": {
                                      "title": "xxx",
                                      "sn": "xxxxx",
                                      "state_id": 1,
                                      "ticket_id": 1,
                                      "gmt_modified": "2018-04-30 15:57:26",
                                      "gmt_created": "2018-04-30 15:56:02",
                                      "xxxx": "....."
                              }
                      },
                      {
                              "state": {
                                      "state_name": "TL审批中",
                                      "state_id": 3
                              },
                              "transition": {
                                      "transition_name": "同意",
                                      "transition_id": 3
                              },
                              "ticket_id": 1,
                              "participant_info": {
                                      "participant_email": "lilei@163.com",
                                      "participant_alias": "李磊",
                                      "participant_phone": "13888888888",
                                      "participant": "lilei",
                                      "participant_type_id": 1
                              },
                              "gmt_modified": "2018-04-30 15:57:00",
                              "gmt_created": "2018-04-30 15:53:19",
                              "suggestion": "同意处理",
                              "ticket_data": {
                                      "title": "xxx",
                                      "sn": "xxxxx",
                                      "state_id": 1,
                                      "ticket_id": 1,
                                      "gmt_modified": "2018-04-30 15:57:26",
                                      "gmt_created": "2018-04-30 15:56:02",
                                      "xxxx": "....."
                              }
                      },
                      {
                              "state": {
                                      "state_name": "新建中",
                                      "state_id": 1
                              },
                              "transition": {
                                      "transition_name": "提交",
                                      "transition_id": 1
                              },
                              "ticket_id": 1,
                              "gmt_modified": "2018-04-30 15:52:35",
                              "gmt_created": "2018-04-10 17:39:33",
                              "suggestion": "请尽快处理，谢谢",
                              "ticket_data": {
                                      "title": "xxx",
                                      "sn": "xxxxx",
                                      "state_id": 1,
                                      "ticket_id": 1,
                                      "gmt_modified": "2018-04-30 15:57:26",
                                      "gmt_created": "2018-04-30 15:56:02",
                                      "xxxx": "....."
                              }
                      }
              ],
              "page": 1,
              "per_page": 10
      },
      "code": 0
```

### 工单处理步骤记录

- url：api/v1/service/steps/`id` 

- method：get

- 请求参数

无

- 返回数据

```json
{
  "data": {
    "current_state_id": 2  //工单当前状态id
    "value": [{
      "state_id": 17,
      "state_flow_log_list": [],
      "order_id": 0,
      "state_name": "test11111"
    }, {
      "state_id": 18,
      "state_flow_log_list": [],
      "order_id": 0,
      "state_name": "2233222"
    }, {
      "state_id": 6,
      "state_flow_log_list": [{
        "gmt_created": "2018-05-15 07:16:38",
        "participant_info": {
          "participant_alias": "李磊",
          "participant_type_id": 1,
          "participant": "lilei",
          "participant_phone": "13888888888",
          "participant_email": "lilei@163.com"
        },
        "suggestion": "",
        "participant": "lilei",
        "state_id": 6,
        "participant_type_id": 1,
        "transition": {
          "transition_name": "提交",
          "transition_id": 7
        },
        "id": 32,
        "intervene_type_id": 0
      }],
      "order_id": 1,
      "state_name": "发起人-新建中"
    }, {
      "state_id": 7,
      "state_flow_log_list": [{
        "gmt_created": "2018-05-15 07:20:40",
        "participant_info": {
          "participant_alias": "李磊",
          "participant_type_id": 1,
          "participant": "lilei",
          "participant_phone": "13888888888",
          "participant_email": "lilei@163.com"
        },
        "suggestion": "同意申请",
        "participant": "lilei",
        "state_id": 7,
        "participant_type_id": 1,
        "transition": {
          "transition_name": "同意",
          "transition_id": 8
        },
        "id": 33,
        "intervene_type_id": 0
      }],
      "order_id": 2,
      "state_name": "发起人tl-审批中"
    }, {
      "state_id": 8,
      "state_flow_log_list": [{
        "gmt_created": "2018-05-16 06:42:00",
        "participant_info": {
          "participant_alias": "轨迹",
          "participant_type_id": 1,
          "participant": "guiji",
          "participant_phone": "13888888888",
          "participant_email": "guiji@163.com"
        },
        "suggestion": "接单处理",
        "participant": "guiji",
        "state_id": 8,
        "participant_type_id": 1,
        "transition": {
          "transition_name": "未知操作",
          "transition_id": 0
        },
        "id": 36,
        "intervene_type_id": 0
      }, {
        "gmt_created": "2018-05-16 06:49:55",
        "participant_info": {
          "participant_alias": "轨迹",
          "participant_type_id": 1,
          "participant": "guiji",
          "participant_phone": "13888888888",
          "participant_email": "guiji@163.com"
        },
        "suggestion": "同意",
        "participant": "guiji",
        "state_id": 8,
        "participant_type_id": 1,
        "transition": {
          "transition_name": "同意",
          "transition_id": 9
        },
        "id": 37,
        "intervene_type_id": 0
      }, {
        "gmt_created": "2018-05-16 06:57:31",
        "participant_info": {
          "participant_alias": "轨迹",
          "participant_type_id": 1,
          "participant": "guiji",
          "participant_phone": "13888888888",
          "participant_email": "guiji@163.com"
        },
        "suggestion": "接单处理",
        "participant": "guiji",
        "state_id": 8,
        "participant_type_id": 1,
        "transition": {
          "transition_name": "未知操作",
          "transition_id": 0
        },
        "id": 38,
        "intervene_type_id": 0
      }, {
        "gmt_created": "2018-05-16 06:57:36",
        "participant_info": {
          "participant_alias": "轨迹",
          "participant_type_id": 1,
          "participant": "guiji",
          "participant_phone": "13888888888",
          "participant_email": "guiji@163.com"
        },
        "suggestion": "同意",
        "participant": "guiji",
        "state_id": 8,
        "participant_type_id": 1,
        "transition": {
          "transition_name": "同意",
          "transition_id": 9
        },
        "id": 39,
        "intervene_type_id": 0
      }, {
        "gmt_created": "2018-05-16 06:58:41",
        "participant_info": {
          "participant_alias": "轨迹",
          "participant_type_id": 1,
          "participant": "guiji",
          "participant_phone": "13888888888",
          "participant_email": "guiji@163.com"
        },
        "suggestion": "同意",
        "participant": "guiji",
        "state_id": 8,
        "participant_type_id": 1,
        "transition": {
          "transition_name": "同意",
          "transition_id": 9
        },
        "id": 40,
        "intervene_type_id": 0
      }, {
        "gmt_created": "2018-05-16 07:01:53",
        "participant_info": {
          "participant_alias": "轨迹",
          "participant_type_id": 1,
          "participant": "guiji",
          "participant_phone": "13888888888",
          "participant_email": "guiji@163.com"
        },
        "suggestion": "同意",
        "participant": "guiji",
        "state_id": 8,
        "participant_type_id": 1,
        "transition": {
          "transition_name": "同意",
          "transition_id": 9
        },
        "id": 41,
        "intervene_type_id": 0
      }, {
        "gmt_created": "2018-05-16 07:03:34",
        "participant_info": {
          "participant_alias": "轨迹",
          "participant_type_id": 1,
          "participant": "guiji",
          "participant_phone": "13888888888",
          "participant_email": "guiji@163.com"
        },
        "suggestion": "同意",
        "participant": "guiji",
        "state_id": 8,
        "participant_type_id": 1,
        "transition": {
          "transition_name": "同意",
          "transition_id": 9
        },
        "id": 43,
        "intervene_type_id": 0
      }, {
        "gmt_created": "2018-05-16 07:04:45",
        "participant_info": {
          "participant_alias": "轨迹",
          "participant_type_id": 1,
          "participant": "guiji",
          "participant_phone": "13888888888",
          "participant_email": "guiji@163.com"
        },
        "suggestion": "同意",
        "participant": "guiji",
        "state_id": 8,
        "participant_type_id": 1,
        "transition": {
          "transition_name": "同意",
          "transition_id": 9
        },
        "id": 45,
        "intervene_type_id": 0
      }, {
        "gmt_created": "2018-05-16 07:31:29",
        "participant_info": {
          "participant_alias": "轨迹",
          "participant_type_id": 1,
          "participant": "guiji",
          "participant_phone": "13888888888",
          "participant_email": "guiji@163.com"
        },
        "suggestion": "同意",
        "participant": "guiji",
        "state_id": 8,
        "participant_type_id": 1,
        "transition": {
          "transition_name": "同意",
          "transition_id": 9
        },
        "id": 47,
        "intervene_type_id": 0
      }, {
        "gmt_created": "2018-05-16 23:21:00",
        "participant_info": {
          "participant_alias": "轨迹",
          "participant_type_id": 1,
          "participant": "guiji",
          "participant_phone": "13888888888",
          "participant_email": "guiji@163.com"
        },
        "suggestion": "同意",
        "participant": "guiji",
        "state_id": 8,
        "participant_type_id": 1,
        "transition": {
          "transition_name": "同意",
          "transition_id": 9
        },
        "id": 49,
        "intervene_type_id": 0
      }, {
        "gmt_created": "2018-05-16 23:24:03",
        "participant_info": {
          "participant_alias": "轨迹",
          "participant_type_id": 1,
          "participant": "guiji",
          "participant_phone": "13888888888",
          "participant_email": "guiji@163.com"
        },
        "suggestion": "同意",
        "participant": "guiji",
        "state_id": 8,
        "participant_type_id": 1,
        "transition": {
          "transition_name": "同意",
          "transition_id": 9
        },
        "id": 51,
        "intervene_type_id": 0
      }, {
        "gmt_created": "2018-05-16 23:24:44",
        "participant_info": {
          "participant_alias": "轨迹",
          "participant_type_id": 1,
          "participant": "guiji",
          "participant_phone": "13888888888",
          "participant_email": "guiji@163.com"
        },
        "suggestion": "同意",
        "participant": "guiji",
        "state_id": 8,
        "participant_type_id": 1,
        "transition": {
          "transition_name": "同意",
          "transition_id": 9
        },
        "id": 53,
        "intervene_type_id": 0
      }, {
        "gmt_created": "2018-05-16 23:33:26",
        "participant_info": {
          "participant_alias": "轨迹",
          "participant_type_id": 1,
          "participant": "guiji",
          "participant_phone": "13888888888",
          "participant_email": "guiji@163.com"
        },
        "suggestion": "同意",
        "participant": "guiji",
        "state_id": 8,
        "participant_type_id": 1,
        "transition": {
          "transition_name": "同意",
          "transition_id": 9
        },
        "id": 55,
        "intervene_type_id": 0
      }],
      "order_id": 3,
      "state_name": "运维人员-审批中"
    }, {
      "state_id": 9,
      "state_flow_log_list": [{
        "gmt_created": "2018-05-16 07:01:54",
        "participant_info": {
          "participant_phone": "",
          "participant_alias": "demo_script.py",
          "participant_email": "",
          "participant_type_id": 6,
          "participant": "demo_script.py"
        },
        "suggestion": "False\n",
        "participant": "demo_script.py",
        "state_id": 9,
        "participant_type_id": 6,
        "transition": {
          "transition_name": "脚本执行完成",
          "transition_id": 10
        },
        "id": 42,
        "intervene_type_id": 0
      }, {
        "gmt_created": "2018-05-16 07:03:34",
        "participant_info": {
          "participant_phone": "",
          "participant_alias": "demo_script.py",
          "participant_email": "",
          "participant_type_id": 6,
          "participant": "demo_script.py"
        },
        "suggestion": "False\n",
        "participant": "demo_script.py",
        "state_id": 9,
        "participant_type_id": 6,
        "transition": {
          "transition_name": "脚本执行完成",
          "transition_id": 10
        },
        "id": 44,
        "intervene_type_id": 0
      }, {
        "gmt_created": "2018-05-16 07:04:45",
        "participant_info": {
          "participant_phone": "",
          "participant_alias": "demo_script.py",
          "participant_email": "",
          "participant_type_id": 6,
          "participant": "demo_script.py"
        },
        "suggestion": "False\n",
        "participant": "demo_script.py",
        "state_id": 9,
        "participant_type_id": 6,
        "transition": {
          "transition_name": "脚本执行完成",
          "transition_id": 10
        },
        "id": 46,
        "intervene_type_id": 0
      }, {
        "gmt_created": "2018-05-16 07:31:29",
        "participant_info": {
          "participant_phone": "",
          "participant_alias": "demo_script.py",
          "participant_email": "",
          "participant_type_id": 6,
          "participant": "demo_script.py"
        },
        "suggestion": "lilei\n",
        "participant": "demo_script.py",
        "state_id": 9,
        "participant_type_id": 6,
        "transition": {
          "transition_name": "脚本执行完成",
          "transition_id": 10
        },
        "id": 48,
        "intervene_type_id": 0
      }, {
        "gmt_created": "2018-05-16 23:21:00",
        "participant_info": {
          "participant_phone": "",
          "participant_alias": "demo_script.py",
          "participant_email": "",
          "participant_type_id": 6,
          "participant": "demo_script.py"
        },
        "suggestion": "lilei\n",
        "participant": "demo_script.py",
        "state_id": 9,
        "participant_type_id": 6,
        "transition": {
          "transition_name": "脚本执行完成",
          "transition_id": 10
        },
        "id": 50,
        "intervene_type_id": 0
      }, {
        "gmt_created": "2018-05-16 23:24:03",
        "participant_info": {
          "participant_phone": "",
          "participant_alias": "demo_script.py",
          "participant_email": "",
          "participant_type_id": 6,
          "participant": "demo_script.py"
        },
        "suggestion": "lilei\n",
        "participant": "demo_script.py",
        "state_id": 9,
        "participant_type_id": 6,
        "transition": {
          "transition_name": "脚本执行完成",
          "transition_id": 10
        },
        "id": 52,
        "intervene_type_id": 0
      }, {
        "gmt_created": "2018-05-16 23:24:44",
        "participant_info": {
          "participant_phone": "",
          "participant_alias": "demo_script.py",
          "participant_email": "",
          "participant_type_id": 6,
          "participant": "demo_script.py"
        },
        "suggestion": "lilei\n",
        "participant": "demo_script.py",
        "state_id": 9,
        "participant_type_id": 6,
        "transition": {
          "transition_name": "脚本执行完成",
          "transition_id": 10
        },
        "id": 54,
        "intervene_type_id": 0
      }, {
        "gmt_created": "2018-05-16 23:33:26",
        "participant_info": {
          "participant_phone": "",
          "participant_alias": "demo_script.py",
          "participant_email": "",
          "participant_type_id": 6,
          "participant": "demo_script.py"
        },
        "suggestion": "lilei\n",
        "participant": "demo_script.py",
        "state_id": 9,
        "participant_type_id": 6,
        "transition": {
          "transition_name": "脚本执行完成",
          "transition_id": 10
        },
        "id": 56,
        "intervene_type_id": 0
      }],
      "order_id": 4,
      "state_name": "授权脚本-自动执行中"
    }, {
      "state_id": 10,
      "state_flow_log_list": [{
        "gmt_created": "2018-05-17 06:45:58",
        "participant_info": {
          "participant_alias": "李磊",
          "participant_type_id": 1,
          "participant": "lilei",
          "participant_phone": "13888888888",
          "participant_email": "lilei@163.com"
        },
        "suggestion": "请处理",
        "participant": "lilei",
        "state_id": 10,
        "participant_type_id": 1,
        "transition": {
          "transition_name": "转交操作",
          "transition_id": 0
        },
        "id": 57,
        "intervene_type_id": 1
      }, {
        "gmt_created": "2018-05-17 06:47:46",
        "participant_info": {
          "participant_alias": "张三",
          "participant_type_id": 1,
          "participant": "zhangsan",
          "participant_phone": "13888888888",
          "participant_email": "zhangsan@163.com"
        },
        "suggestion": "请协助处理",
        "participant": "zhangsan",
        "state_id": 10,
        "participant_type_id": 1,
        "transition": {
          "transition_name": "加签操作",
          "transition_id": 0
        },
        "id": 58,
        "intervene_type_id": 2
      }],
      "order_id": 6,
      "state_name": "发起人-确认中"
    }, {
      "state_id": 11,
      "state_flow_log_list": [],
      "order_id": 7,
      "state_name": "结束"
    }]
  },
  "msg": "",
  "code": 0
}
```

## 用户列表（分页）

- url：api/v1/account/users

- method：get

- 请求参数

|     请求参数     | 参数类型 |                 参数说明                 |
| :--------------: | :------: | :--------------------------------------: |
|       search      |  String  | 模糊搜索，模糊匹配username，nickname名称|

- 返回数据

```json
{
    "count": 122,
    "next": "http://127.0.0.1:8000/api/v1/account/users/?page=2&serach=",
    "previous": null,
    "results": [
        {
            "id": 1,
            "username": "104315580326534961",
            "mobile": "18729229862",
            "avatar": "",
            "nickname": "梁攀峰"
        },
        {
            "id": 2,
            "username": "175968296423409077",
            "mobile": "15594633606",
            "avatar": "https://static-legacy.dingtalk.com/media/lADPDgQ9q1_wA1HNAhnNAUw_332_537.jpg",
            "nickname": "宋江浩"
        },
    ]
}
```

### 抄送相关接口

#### 抄送列表

url: api/v1/service/cc/

请求方式：GET

请求参数：
| 请求参数  | 类型    | 必填 | 说明     |
| --------- | ------- | ---- | -------- |
| ticket_id | int     | 是   | 工单id   |
| username  | varchar | 是   | 抄送人  |
| is_read   | bool    | 否   | 是否已读 |

返回：
```json
[
    {
        "id": 1,
        "ticket_id": 1,
        "username": "admin",
        "is_read": true,
        "created_time": "2020-04-02 15:41:14"
    },
]
```

#### 创建抄送

url: api/v1/service/cc/

请求方式：POST

请求参数：
| 请求参数  | 类型    | 必填 | 说明     |
| --------- | ------- | ---- | -------- |
| ticket_id | int     | 是   | 工单id   |
| username  | varchar | 是   | 抄送人  |

返回：
```json
{
    "id": 1,
    "username": "104315580326534961",
    "ticket_id": "18729229862",
    "is_read": true,
}
```

#### 创建多条抄送


url: api/v1/service/cc/bulck_cc/

请求方式：POST

请求参数：
| 请求参数  | 类型    | 必填 | 说明     |
| --------- | ------- | ---- | -------- |
| id | int     | 是   | 工单id   |
| extra_info | json | 是   | 抄送人数据  |

返回参数：

{
  "id":11,
  "sn":"loonflow202002020001",
  "title":"xxx",
  "state_id":1,
  ...., //等等工单的所有字段的值
  "extra_info": "xxxx", // 此处如果你配置hook的时候指定了extra_info那么会有这个字段，如果没配置就没这个信息
}

```json
// data示例
[
    {
        type: 'person',
        to: ['username1', 'username2']
    },
    {
        type: 'role',
        to: ['role_id1', 'role_id2']
    },
    {
        type: 'dept',
        to: ['dept_id1, dept_id2']
    }
]
```

返回：
```json
success: {
    code: 0,
    msg: {"users": [{"id": 124, "username": "240345616626300820", "mobile": "18919058536", "email": "", "alias": "\u674e\u6b63\u84c9"}], "type": "cc"}
},
error: {
    code: -1,
    msg: '抄送失败'
}
```

#### 标记抄送已读

url：api/v1/service/cc/cc_read/

请求方式：post
请求参数
| 请求参数  | 类型    | 必填 | 说明     |
| --------- | ------- | ---- | -------- |
| ticket_id | int     | 是   | 工单id   |

返回

```json
success: {
    result: 'info',
    msg: '已读'
},
error: {
    result: 'error',
    msg: '已读失败'
}
```

### 抄送详情

- url：api/v1/service/cc/`id`
- method：get
- 请求参数

无

- 返回数据

```json
{
    "id": 4,
    "ticket_id": 3,
    "username": "admin",
    "is_read": false,
    "created_time": "2020-04-02 18:04:07"
}
```


### 钉钉鉴权相关

### 获取access_token

- url：api/get_access_token/

- method：get

- 请求参数
    无
- 使用场景


- 返回数据

```json
{
    "access_token": "08416c1c666d03e918351a30791ba15ec"
}

### 获取js鉴权参数

- url：api/get_js_api/

- method：get

- 请求参数
    无
- 使用场景

- 返回数据

```json
{
    "corpId": "ding3932ec528b2d22c735c2f4657eb6378f",
    "timeStamp": 1587445638159,
    "nonceStr": "YsRImfQi8zO1bqTh",
    "signature": "f28a8b39d6f00192f802b1c42ebf0afe0e7679d0",
    "agentId": 290317870
}
