#!/bin/sh

echo "stoping uwsgi ..."
cd /workspace/howflow/backend
pipenv run uwsgi --stop ./uwsgi.pid
echo "stoping uwsgi finished."

echo "git configging ... "
git config core.filemode false
echo "git configging finished. "

echo "git pulling code from oschina ... "
cd /workspace/howflow
git pull
echo "git pulling code from oschina finished."

echo "chmoding ... "
chmod -R 777 /workspace/howflow/backend
echo "chmoding finished."

echo "collectting static files ..."
cd /workspace/howflow/backend
pipenv run python manage.py collectstatic --noinput
echo "collectting static files finished."

echo "restarting celery... "
pipenv run celery multi restart w1 -A celery_tasks.main -l info -c 1
echo "restart celery finished."

echo "starting uwsgi ..."
pipenv run uwsgi --ini /workspace/howflow/deploy/uwsgi/uwsgi_howflow.ini
echo "starting uwsgi finished."
