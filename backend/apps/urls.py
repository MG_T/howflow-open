"""apps URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.conf.urls.static import serve, static
from django.conf import settings
from django.urls import path, include
from django.views.generic import TemplateView
from account.views import StsAuth
from ding_platform.views import DingCallbackView, SyncDataView, user_login, get_dingtalk_code, dd_login, JsApiView, AccessTokenApiView

urlpatterns = [
    path("", TemplateView.as_view(template_name="index.html"), name="home"),
    path('admin/', admin.site.urls),
    url(r'^ding_callback/$', DingCallbackView.as_view(), name='ding_callback'),
    url(r'^sync_data/$', SyncDataView.as_view(), name='sync_data'),
    url(r'^api/sts-auth/$', StsAuth.as_view(), name="sts-auth"),
    url(r'^login/', user_login, name='login'),  # 钉钉扫码登录页
    url(r'^get_dingtalk_code/', get_dingtalk_code, name='get_dingtalk_code'),  # 扫码登录回调
    url(r'^dd_login/', dd_login, name='dd_login'),  # 钉钉app code登录
    url(r'^api/get_js_api/', JsApiView.as_view(), name='get_js_api'),
    url(r'^api/get_access_token/', AccessTokenApiView.as_view(), name='get_access_token'),
    path('api/v1/account/', include('account.urls')),
    path('api/v1/service/', include('service.urls')),
    path(r'api-auth/', include('rest_framework.urls',
                               namespace='rest_framework')),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
