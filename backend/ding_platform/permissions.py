# coding: utf-8

from rest_framework import permissions

from config.settings.base import CWSP_IP


class CwspPermission(permissions.BasePermission):
    """
    财务审批白名单
    """

    def has_permission(self, request, view):
        ip_addr = request.META['REMOTE_ADDR']
        return ip_addr == CWSP_IP
