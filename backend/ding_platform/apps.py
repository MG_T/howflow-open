# -*- coding: utf-8 -*-


from django.apps import AppConfig


class DingPlatformConfig(AppConfig):
    name = 'ding_platform'
