import os
import sys

import django
import platform

SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
APPS_DIR = os.path.dirname(SCRIPT_DIR)
BASE_DIR = os.path.dirname(APPS_DIR)
sys.path.insert(0, SCRIPT_DIR)
sys.path.insert(0, BASE_DIR)
sys.path.insert(0, APPS_DIR)

if platform.system() == 'Linux':
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings.production")
else:
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings.local")
django.setup()
