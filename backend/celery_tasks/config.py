# -*- coding: utf-8 -*-
BROKER_URL = "redis://127.0.0.1/14"   # 任务存储
CELERY_RESULT_BACKEND = "redis://127.0.0.1/15"  # 结果存储
CELERY_TASK_RESULT_EXPIRES = 60 * 60  # celery任务执行结果的过期时间一小时
CELERYD_CONCURRENCY = 1   # 并发worker数 也是命令行-c 1指定的数目
CELERYD_MAX_TASKS_PER_CHILD = 300  # 每个worker最多执行300个任务就会被销毁，可防止内存泄露
CELERYD_FORCE_EXECV = True  # 防止调用mysql造成死锁
CELERY_ACCEPT_CONTENT = ['application/json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
CELERY_TIMEZONE = 'Asia/Shanghai'  # 设置时区
CELERY_ENABLE_UTC = True  # 启动时区设置

# CELERY_ENABLE_UTC = False
# CELERYD_PREFETCH_MULTIPLIER = 4  # celery worker 每次去redis取任务的数量 默认4个
# CELERY_CREATE_MISSING_QUEUES = True  # 某个程序中出现的队列，在broker中不存在，则立刻创建它
# FORKED_BY_MULTIPROCESSING = 1  # 4.2 适配win10
# CELERYD_TASK_SOFT_TIME_LIMIT = 600   # 单个任务的运行时间不超过此值，否则会被SIGKILL 信号杀死
# CELERY_TASK_RESULT_EXPIRES = 600    # 任务过期时间,celery任务执行结果的超时时间
# CELERY_DISABLE_RATE_LIMITS = True  # 任务发出后，经过一段时间还未收到acknowledge , 就将任务重新交给其他worker执行
# 设置默认的队列名称，如果一个消息不符合其他的队列就会放在默认队列里面，如果什么都不设置的话，数据都会发送到默认的队列中
# CELERY_DEFAULT_QUEUE = "default"
# 设置详细的队列
# CELERY_QUEUES = {
#     "default": {  # 这是上面指定的默认队列
#         "exchange": "default",
#         "exchange_type": "direct",
#         "routing_key": "default"
#     },
#     "topicqueue": {  # 这是一个topic队列 凡是topictest开头的routing key都会被放到这个队列
#         "routing_key": "topic.#",
#         "exchange": "topic_exchange",
#         "exchange_type": "topic",
#     },
#     "task_eeg": {  # 设置扇形交换机
#         "exchange": "tasks",
#         "exchange_type": "fanout",
#         "binding_key": "tasks",
#     },
#
# }

# 定时任务

# CELERYBEAT_SCHEDULE = {
#     'msg_notify': {
#         'task': 'async_task.notify.msg_notify',
#         'schedule': timedelta(seconds=10),
#         # 'args': (redis_db),
#         'options': {'queue': 'my_period_task'}
#     },
#     'report_result': {
#         'task': 'async_task.tasks.report_result',
#         'schedule': timedelta(seconds=10),
#         # 'args': (redis_db),
#         'options': {'queue': 'my_period_task'}
#     },
#     'report_retry': {
#        'task': 'async_task.tasks.report_retry',
#        'schedule': timedelta(seconds=60),
#        'options' : {'queue':'my_period_task'}
#     },

# }

