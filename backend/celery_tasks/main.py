# -*- coding: utf-8 -*-

from celery import Celery

import os


if not os.getenv('DJANGO_SETTINGS_MODULE'):
    os.environ['DJANGO_SETTINGS_MODULE'] = 'config.settings.local'

celery_app = Celery('howflow')

# 导入配置文件
celery_app.config_from_object('celery_tasks.config')

# 自动注册celery任务
celery_app.autodiscover_tasks(['celery_tasks.sms', ])


# 开启celery的命令
#  celery -A 应用路径（.包路径） worker -l info
#  celery -A celery_tasks.main worker -l debug
#  celery multi start w1 -A celery_tasks.main -l info
#  celery multi restart w1 -A celery_tasks.main -l info
#  celery -A celery_tasks.main worker -l info -c 1 --pool=solo  # windows本地调试命令

