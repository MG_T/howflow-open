import traceback
from celery_tasks.main import celery_app
from common.utils import SendMsg
from config.settings.base import celery_sms_logger


# soft_time_limit=5 等待时间6s
# max_retries=3 尝试次数
@celery_app.task(name='send_sms', max_retries=1, soft_time_limit=10)
def send_print_file(username, media_url):
    """
    发送打印文件
    :param username:
    :param media_url:
    :return: None
    """
    # 发送短信
    try:
        celery_sms_logger.info(media_url)
        SendMsg.send_print(username, media_url)
        status = True
    except Exception as e:
        celery_sms_logger.error(traceback.format_exc())
        status = False
    return status

