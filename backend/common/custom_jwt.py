from account.serializers import FetchAccountUserSerializer


def jwt_response_payload_handler(token, user=None, request=None):
    return {
        'token': token,
        'user': FetchAccountUserSerializer(user, context={'request': request}).data
    }



