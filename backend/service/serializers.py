from rest_framework import serializers

from service.models import Cc


class LoonFlowAttachmentSerializer(serializers.Serializer):
    attachment = serializers.FileField(allow_null=True, use_url=True)


class CcSerializer(serializers.ModelSerializer):
    """
    抄送serializer
    """

    class Meta:
        model = Cc
        fields = "__all__"
