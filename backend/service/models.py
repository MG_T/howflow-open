from django.utils import timezone

from django.db import models

#
# class TicketAttachment(models.Model):
#     attachment = models.FileField(upload_to='upload/%Y-%m')


class Cc(models.Model):
    """
    抄送
    """
    ticket_id = models.IntegerField(verbose_name='工单id')
    username = models.CharField(verbose_name='抄送人', max_length=200)
    is_read = models.BooleanField(verbose_name='是否已读', default=False)
    created_time = models.DateTimeField(verbose_name='创建时间', default=timezone.now)

    class Meta:
        verbose_name = '抄送'
        verbose_name_plural = verbose_name
