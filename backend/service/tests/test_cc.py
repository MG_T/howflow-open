import datetime
import json

from rest_framework.reverse import reverse

from account.tests.test_user import TestClassWithLogin
from service.models import Cc
from service.tests.factories import CcFactory


class UserTest(TestClassWithLogin):
    """
    抄送相关测试用例
    """
    url = reverse('cc-list')

    def test_cc_list(self):
        """
        抄送-列表测试用例
        """
        for _ in range(10):
            CcFactory()
        url = self.url
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, 200)
        content = json.loads(response.content)
        self.assertEqual(len(content), 10)

    def test_cc_detail(self):
        """
        抄送-详情测试用例
        """
        obj = CcFactory(username='test_user')
        url = self.url + '{}/'.format(obj.id)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        content = json.loads(response.content)
        self.assertEqual(content['username'], 'test_user')

    def test_cc_create(self):
        """
         抄送-创建测试用例
        """
        data = {
            'ticket_id': 1,
            'username': 'test_user',
            'created_time': datetime.datetime.now(),
            'is_read': False,
        }
        url = self.url
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 201)
        content = json.loads(response.content)
        self.assertEqual(content['ticket_id'], 1)
        self.assertEqual(content['username'], 'test_user')

        """
        抄送-已读
        """
        r_data = {
            "ticket_id": 1,
        }
        url = self.url + 'cc_read/'
        response = self.client.post(url, data=r_data)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(Cc.objects.get(ticket_id=1).is_read)

    def test_bulck_cc(self):
        """
        抄送-创建多条抄送记录
        """
        flag = '111'
        if not flag:
            data = {
                    "id": 1,
                    "extra_info": [{"type": "person", "to": ["test_user", "223703516326313962"]}],
                   }
            url = self.url + 'bulck_cc/'
            response = self.client.post(url, data=json.dumps(data), content_type="application/json")
            self.assertEqual(response.status_code, 200)
            self.assertEqual(2, len(Cc.objects.filter(ticket_id=1).values_list("username", flat=True)))
