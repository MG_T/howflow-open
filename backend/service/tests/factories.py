import datetime
import factory
import factory.fuzzy
from factory import DjangoModelFactory

from service.models import Cc


class CcFactory(DjangoModelFactory):
    """
    抄送factory
    """
    ticket_id = factory.fuzzy.FuzzyInteger(1, 10)
    username = factory.Sequence(lambda n: "test username {}".format(n))
    is_read = factory.fuzzy.FuzzyChoice([False, True])
    created_time = factory.LazyFunction(datetime.datetime.now)

    class Meta:
        model = Cc
