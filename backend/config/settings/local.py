﻿from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# 非Django库代码的记录日志级别调整为'DEBUG'
LOGGING['handlers']['my_file']['level'] = 'DEBUG'

# 打印输出日志
LOGGING['loggers']['django']['handlers'].append('django_file_stream')

# STATIC
# ------------------------
# https://django-storages.readthedocs.io/en/latest/backends/amazon-S3.html#settings
# from storages.backends.s3boto3 import S3Boto3Storage  # noqa E402


# class MediaRootS3Boto3Storage(S3Boto3Storage):
#     location = "media"
#     file_overwrite = False


DEFAULT_FILE_STORAGE = "config.settings.local.MediaRootS3Boto3Storage"
MEDIA_URL = f"https://{AWS_STORAGE_BUCKET_NAME}.cos.ap-chengdu.myqcloud.com/media/"


# Database
# https://docs.djangoproject.com/en/2.0/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'HOST': '127.0.0.1',
        'NAME': 'zlaw_wf',
        'USER': 'zlaw_wf_rw',
        'PASSWORD': '6zlawCS#@!123',
        'PORT': 3306,
        'OPTIONS': {
            'init_command': 'SET sql_mode=STRICT_TRANS_TABLES',
        },
    }
}
