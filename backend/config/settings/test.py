from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

# Database
# https://docs.djangoproject.com/en/2.0/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'xxxxxx',
        'USER': 'xxxxxx',
        'PASSWORD': 'xxxxxx',
        'HOST': 'xxxxxx.cdb.myqcloud.com',
        'PORT': 'xxxxxx',
        'OPTIONS': {
            'init_command': 'SET sql_mode=STRICT_TRANS_TABLES',
        },
    }
}


from storages.backends.s3boto3 import S3Boto3Storage  # noqa E402


class MediaRootS3Boto3Storage(S3Boto3Storage):
    location = "media"
    file_overwrite = False


DEFAULT_FILE_STORAGE = "config.settings.local.MediaRootS3Boto3Storage"
MEDIA_URL = f"https://{AWS_STORAGE_BUCKET_NAME}.cos.ap-chengdu.myqcloud.com/media/"

