from django.urls import path
from rest_framework.routers import DefaultRouter
from account.views import UserViewSet
from account.views import ShutongObtainJSONWebToken

router = DefaultRouter()
router.register('users', UserViewSet, 'users')

urlpatterns = [
    path('obtain_token/', ShutongObtainJSONWebToken.as_view())
]

urlpatterns += router.urls