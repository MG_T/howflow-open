﻿from django.contrib.auth import admin as auth_admin
from django.contrib.auth import get_user_model
from django.contrib import admin
# Register your models here.


admin.site.site_title = "howflow项目管理"
admin.site.site_header = "后台管理"
admin.site.index_title = "管理首页"

User = get_user_model()


@admin.register(User)
class UserAdmin(auth_admin.UserAdmin):
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        ('个人信息', {'fields': (
            'nickname', 'mobile', 'avatar',
        )}),
        ('权限', {'fields': (
            'is_active', 'is_staff', 'is_superuser',
            'groups', 'user_permissions'
        )}),
        ('系统记录', {'fields': ('last_login', 'date_joined')}),
    )

    list_display = (
        'username', 'nickname', 'mobile',
        'is_active', 'is_staff', 'is_superuser'
    )
    search_fields = (
        'username', 'nickname', 'mobile',
    )
    list_filter = ('nickname', 'mobile',)


