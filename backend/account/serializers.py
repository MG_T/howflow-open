from account.models import User
from rest_framework import serializers


class FetchAccountUserSerializer(serializers.ModelSerializer):

    # loonflow中，没有 nickname 和 avatar 两个字段。 为了全局统一这里使用一致的字段名
    alias = serializers.CharField(source='nickname')
    email = serializers.CharField(source='avatar')

    class Meta:
        model = User
        fields = ['id', 'username', 'mobile', 'email', 'alias']
