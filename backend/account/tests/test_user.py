import json

from django.test import TestCase
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from account.tests.factories import OrgFactory, UserFactory


class TestClassWithBaseData(TestCase):

    # 添加测试用户
    def setUp(self):
        self.org = OrgFactory(name='测试部门')
        self.test_user = UserFactory(username='test_user')
        self.test_user.set_password('666666')
        self.test_user.save()
        self.prepare()

    def prepare(self):
        pass


class TestClassWithLogin(APITestCase, TestClassWithBaseData):
    """
    需要用户登录的测试用例
    """
    def prepare(self):
        username = self.test_user.username
        self.client.post(
            '/api-auth/login/', {'username': username, 'password': '666666'})
        self.login_user = self.test_user


class UserTest(TestClassWithLogin):
    """
    用户相关测试用例
    """
    url = reverse('users-list')

    def test_users_list(self):
        """
        测试用户列表展示
        """
        for _ in range(10):
            UserFactory()
        url = self.url
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, 200)
        content = json.loads(response.content)
        self.assertEqual(content['count'], 11)
