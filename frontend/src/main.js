// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import '@/assets/font/iconfont.css'
import router from './router'
import store from './store/store.js'
import utils from './utils/utils'

import createdTicketPdf from './utils/createdTicketPdf.js'
Vue.use(createdTicketPdf)

// 联系人
// import contact from './components/contact.js'
// Vue.prototype.$contact = contact

import axios from 'axios'
if (process.env.NODE_ENV === 'development') {
  axios.defaults.baseURL = 'http://127.0.0.1:8000'
}else if(process.env.NODE_ENV === 'production'){
  axios.defaults.baseURL = ''
}
axios.interceptors.request.use(config => {
  return config
}, error => {
  return Promise.reject(error)
})


Vue.prototype.$store =  store
Vue.prototype.$U = utils
Vue.prototype.$axios =  axios
import './ui/index'


import FastClick from 'fastclick'
import * as dd from 'dingtalk-jsapi';
import 'lib-flexible'
FastClick.attach(document.body)
Vue.config.productionTip = false

import filters from './filter/filter'
Vue.filter('dateFormat',filters.dateFormat)

dd.error((error)=>{
  alert('dd error: ' + JSON.stringify(error))
})
Vue.prototype.$dd = dd

var rem = function () {
  var html = document.documentElement;
  var width = html.getBoundingClientRect().width;
  html.style.fontSize = width / 10 + 'px'
};
rem();
window.onresize = function(){
  rem();
}

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
