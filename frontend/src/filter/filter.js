import moment from 'moment'
function dateFormat (value,format='YYYY-MM-DD') {
  return moment(value).format(format);
}
export default{
  dateFormat
}


