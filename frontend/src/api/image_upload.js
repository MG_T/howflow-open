// 获取文件直传 XCosSecurityToken
import store from '../store/store'
import ahr from './axios_cos'

// PUT 请求 验证 Key --> 文件上传路径   url --> Prefix + key

export function uploadFile(data, callback) {
  let Key = data.Key
  let file = data.file
   console.log(Key)
  store.dispatch('getAuthorization', {Method: 'PUT', Key: Key}).then(res => {
    if(res.status == 200){
      let Prefix = res.data.Prefix;
      let url = Prefix + Key
      let Authorization = res.data.Authorization;
      let XCosSecurityToken = res.data.XCosSecurityToken;
      ahr({
        url: Prefix + Key,
        method: 'PUT',
        headers: {
          'Content-Type': 'image/jpeg',
          'Authorization': Authorization,
          'x-cos-security-token': XCosSecurityToken,
        },
        data:file
      }).then(res => {
        console.log(res)
        if (res.status == 200) {
          callback(null, {url: url})
        }else {
           callback('上传图片失败请刷新！');
        }
      }).catch(err => {
        callback('上传图片失败请刷新！');
      })
    } else {
      callback('获取签名出错');
    }
  }).catch(err => {
    callback('获取签名出错');
  });
}


// post 请求 验证 Key --> ''  url --> Prefix  表单提交

// export function uploadFile(data, callback) {
//   getAuthorization({Method: 'POST', Key: ''}).then(res => {
//     for (let i = 0; i < data.files.length; i++) {
//       let filename = new Date().getTime() + data.files[i].file.name
//       let uploadPath = basePath + data.uploadPath + filename;
//       let Key = 'media' + '/' + uploadPath;
//       if (res.status == 200) {
//         let Prefix = res.data.Prefix;
//         let url = Prefix + Key
//         let Authorization = res.data.Authorization;
//         let XCosSecurityToken = res.data.XCosSecurityToken;
//         let formDate = new FormData();
//         formDate.append('file', data.files[i].file)
//         formDate.append('key', Key)
//         formDate.append('success_action_status', 200)
//         formDate.append('Signature', Authorization)
//         formDate.append('x-cos-security-token', XCosSecurityToken)
//         ahr({
//           url: Prefix,
//           method: 'POST',
//           headers: {
//             'Content-Type': 'multipart/form-data',
//             'Authorization': Authorization,
//             'x-cos-security-token': XCosSecurityToken,
//           },
//           data: formDate
//         }).then(res => {
//           console.log(res)
//           if (res.status == 200) {
//             callback(null, {url: url, uploadPath: uploadPath})
//           }
//         }).catch(err => {
//           callback('上传图片失败请刷新！');
//         })
//       } else {
//         callback('获取签名出错');
//       }
//     }
//   }).catch(err => {
//     callback('获取签名出错');
//   });
// }



export function deleteFile(data, callback) {
  let Key = 'media' + '/' + data.filePath;
  console.log(Key)
  store.dispatch('getAuthorization', {Method: 'DELETE', Key: Key}).then(res => {
    console.log(res.data)
    let Prefix = res.data.Prefix;
    let Authorization = res.data.Authorization;
    let XCosSecurityToken = res.data.XCosSecurityToken;
    ahr({
      url: Prefix + Key,
      method: 'DELETE',
      headers:{
        'Authorization': Authorization,
        'x-cos-security-token': XCosSecurityToken,
      },
    }).then(res=>{
       console.log(res)
      if(res.status == 204){
        callback(null, res.config);
      }else{
       callback('删除文件败，状态码：' + res.status);
     }
    }).catch(err =>{
        callback('删除文件败');
    })
  }).catch(err => {
      callback('获取签名出错');
  });
}
