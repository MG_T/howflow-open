import axios from 'axios';
import store from '../store/store'
import utils from '../utils/utils'
// const service = axios.create()
const service = axios

let baseUrl = ''

if (process.env.NODE_ENV === 'development') {
  baseUrl = 'http://127.0.0.1:6062/'
} else if (process.env.NODE_ENV === 'production') {
  baseUrl = '';
}

// service.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
// service.defaults.headers.post['Content-Type'] = "application/json;charset=utf-8"

service.defaults.withCredentials = true

service.defaults.baseURL = baseUrl;

service.interceptors.request.use(function (config) {
 
  // 请求开始
  return config
},
err => {
  store.state.isLoading = false 
  if (err.response) {
    if (err.response.status == 401) {
      
    }
  }
  return Promise.reject(err) // 返回接口返回的错误信息
})

service.interceptors.response.use(function (response) {
  // 请求结束
  return response
})
export default service;
