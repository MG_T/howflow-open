import axios from './axios_config'

// 获取JSAPI鉴权所需要的配置信息(nonceStr，agentId，timeStamp，corpId，signature)
export function getJsapiTicket() {
  return axios({
    url: `/api/get_js_api/`,
    method: 'get',
  })
}

// 账号密码登陆
export function login() {
  return axios({
    url: `/api/v1/account/obtain_token/`,
    method: 'post',
    data:{
      username: "xxxxxx",
      password: "xxxxxx"
    }
  })
}
