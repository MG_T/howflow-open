import axios from 'axios';
const service = axios.create()

let baseUrl = ''

if (process.env.NODE_ENV === 'development') {
  baseUrl = 'http://127.0.0.1:8000';
} else if (process.env.NODE_ENV === 'production') {
  baseUrl = '';
}

service.defaults.headers.post['Content-Type'] = 'multipart/form-data'

service.defaults.withCredentials = true

service.defaults.baseURL = baseUrl;


export default service;
