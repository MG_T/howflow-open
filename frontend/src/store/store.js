import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios'
import utils from '../utils/utils'

Vue.use(Vuex);

let corpId, appId
if (process.env.NODE_ENV === 'development') {
    corpId = 'xxxxxx';
    appId = 'xxxxxx';
}else if (process.env.NODE_ENV === 'production') {
    corpId = 'xxxxxx';
    appId = 'xxxxxx';
}

const store = new Vuex.Store({
  state: {
    corpId: corpId,
    appId: appId,
    is_cache:true,
    history_list:[],
    loading:false,
  },

  getters:{
    corpId: state => state.corpId,
    appId: state => state.appId,
    is_cache: state => state.is_show,
  },

  mutations:{
    addHistory:(state,val) => {
      if(state.history_list.indexOf(val) >= 0){
        let index = state.history_list.indexOf(val)
       state.history_list.splice(index,1)
      }
      if(val){
        state.history_list.unshift(val)
      }
      if(state.history_list.length > 7){
        state.history_list.pop()
      }
    }
  },
  actions:{
    // 登录
    login: (context, data) => {
      return new Promise((resolve, reject) => {
        axios.post('/api/v1/account/obtain_token/', data).then(resp => {
          console.log(resp)
          axios.defaults.headers.Authorization = 'JWT ' + resp.data.token
          resolve(resp)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 验证
    getAuthorization: (context, data) => {
      let method = (data.Method || 'get').toLowerCase();
      let key = data.Key || '';
      console.log(key)
      let pathname = key.indexOf('/') === 0 ? key : '/' + key;
      let url = '/api/sts-auth/' +
                '?method=' + method + '&pathname=' +
                encodeURIComponent(pathname);
      return axios({
        url: url,
        method: 'get',
      })
    },
    // 用户列表
    users: (context,params) => {
      return new Promise((resolve, reject) => {
        axios.get(`/api/v1/account/users/?page=${params.page}&search=${params.search}`,params).then(resp => {
          resolve(resp)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 工单初始化状态
    api_init_state: (context, params) => {
      return new Promise((resolve, reject) => {
        axios.get(`/api/v1/service/init_state/${params.id}/`).then(resp => {
          resolve(resp)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 获取流程列表
    api_workflows: (context, data) => {
      return new Promise((resolve, reject) => {
        axios.get(`/api/v1/service/workflows/?page=${data.page}`).then(resp => {
          resolve(resp)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 提交工单
    api_post_ticket: (context, data) => {
      return new Promise((resolve, reject) => {
        axios.post('/api/v1/service/create_ticket/', data).then(resp => {
          resolve(resp)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 修改工单
    api_post_reviseTicket: (context, data) => {
      return new Promise((resolve, reject) => {
        axios.patch(`/api/v1/service/tickets/${data.id}/`, data).then(resp => {
          resolve(resp)
        }).catch(error => {
          reject(error)
        })
      })
    },
    //获取工单列表
    api_get_ticket_list: (context, params) => {
      let url = '/api/v1/service/tickets/' + utils.toUrl(params)
      return new Promise((resolve, reject) => {
        axios.get(url).then(resp => {
          resolve(resp)
        }).catch(error => {
          reject(error)
        })
      })
    },
    //工单详情
    api_get_ticket_detail: (context, id) => {
      let url = `/api/v1/service/tickets/${id}/`
      return new Promise((resolve, reject) => {
        axios.get(url).then(resp => {
          resolve(resp)
        }).catch(error => {
          reject(error)
        })
      })
    },
    //获取工单流转记录
    api_get_ticket_flowlogs: (context, data) => {
      let url = `/api/v1/service/logs/${data.id}/` + utils.toUrl(data)
      return new Promise((resolve, reject) => {
        axios.get(url).then(resp => {
          resolve(resp)
        }).catch(error => {
          reject(error)
        })
      })
    },
    //获取工单可以做的操作
    api_get_ticket_transitions: (context, id) => {
      return new Promise((resolve, reject) => {
        axios.get(`/api/v1/service/actions/${id}/`).then(resp => {
          resolve(resp)
        }).catch(error => {
          reject(error)
        })
      })
    },
    //处理工单
    api_handle_ticket_action: (context, data) => {
      let url = `/api/v1/service/tickets/${data.ticket_id}/`
      return new Promise((resolve, reject) => {
        axios.patch(url, data).then(resp => {
          resolve(resp)
        }).catch(error => {
          reject(error)
        })
      })
    },
    //接单
    api_post_accept: (context, id) => {
      return new Promise((resolve, reject) => {
        axios.post(`api/v1/service/tickets/${id}/accept/`).then(resp => {
          resolve(resp)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 提交评论
    api_post_comment: (context, data) => {
      return new Promise((resolve, reject) => {
        axios.post(`api/v1/service/comments/`,data).then(resp => {
          resolve(resp)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 撤回工单
    api_post_retreat: (context, id) => {
      return new Promise((resolve, reject) => {
        axios.post(`api/v1/service/tickets/${id}/retreat/`).then(resp => {
          resolve(resp)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 关闭工单
    api_post_close: (context, id) => {
      return new Promise((resolve, reject) => {
        axios.post(`api/v1/service/tickets/${id}/close/`).then(resp => {
          resolve(resp)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 转交工单
    api_post_deliver: (context, data) => {
      return new Promise((resolve, reject) => {
        axios.post(`api/v1/service/tickets/${data.ticket_id}/deliver/`,{
          target_username : data.target_username,
          suggestion : data.suggestion,
        }).then(resp => {
          resolve(resp)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 加签工单
    api_post_add_node: (context, data) => {
      return new Promise((resolve, reject) => {
        axios.post(`api/v1/service/tickets/${data.ticket_id}/add_node/`,{
          target_username : data.target_username,
          suggestion : data.suggestion,
        }).then(resp => {
          resolve(resp)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 加签处理完成工单
    api_add_node_end: (context, data) => {
      return new Promise((resolve, reject) => {
        axios.post(`api/v1/service/tickets/${data.ticket_id}/add_node_end/`,{
          suggestion : data.suggestion,
        }).then(resp => {
          resolve(resp)
        }).catch(error => {
          reject(error)
        })
      })
    },
    //获取抄送列表
    api_get_ticket_copy: (context) => {
      return new Promise((resolve, reject) => {
        axios.get(`api/v1/service/cc/`).then(resp => {
          resolve(resp)
        }).catch(error => {
          reject(error)
        })
      })
    },
    //抄送列表标记已读
    api_post_ticket_cc_read: (context,id) => {
      return new Promise((resolve, reject) => {
        axios.post(`api/v1/service/cc/cc_read/`,{
          ticket_id : id
        }).then(resp => {
          resolve(resp)
        }).catch(error => {
          reject(error)
        })
      })
    },
    //给后台上传pdf文件
    api_post_ticketPdf: (context,data) => {
      return new Promise((resolve, reject) => {
        axios.post(`api/v1/service/tickets/to_print/`,data).then(resp => {
          resolve(resp)
        }).catch(error => {
          reject(error)
        })
      })
    },
  }
})

export default store
