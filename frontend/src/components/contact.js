import Vue from 'vue'
import contact from './contact.vue'

// 实例化Vue，render函数创建组件模板

const v = new Vue({
  render(createElement){
      return createElement(contact) 
  }
})

// 渲染DOM，并挂载到body里

v.$mount() //渲染

document.body.appendChild(v.$el) //挂载

// 获取组件的实例
const load = v.$children[0]


//显示
function show(opt){
  // load相当于是在 组件.vue 当中的this
  // show()是methons中的函数
  load.show(opt)
}

function hide(opt){
  load.hide(opt)
}


export default {
  show,hide
}