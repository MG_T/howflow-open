## 登陆

- api/v1/account/obtain_token/
- post
- {
      "username": "xxxxxx",
      "password": "xxxxxx"
  }
- 返回："token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkI........“

## 获取流程列表

- /api/v1/service/workflows/

- get

  - 请求参数

  | 参数名   | 类型    | 必填 | 说明                          |
  | -------- | ------- | ---- | ----------------------------- |
  | page     | int     | 否   | 页码，默认1                   |
  | per_page | int     | 否   | 每页个数，默认10              |
  | name     | varchar | 否   | 支持根据workflow name模糊查询 |

  - 返回数据

  ```javascript
  {
  	"code": 0,
  	"data": {
  		"total": 2,
  		"page": 1,
  		"per_page": 10,
  		"value": [{
  			id: 1，
        name: "报销"，
        description: "财务"，// 描述（需要作为分类使用 例如：财务下面是：报销，交发票）
        notices: "2"，//通知
        view_permission_check: false，//查看权限检查
        limit_expression: "{}"，
        display_form_str: "["sn", "title", "leave_start", "leave_end", "leave_days",...]"
        creator: "admin"
        gmt_created: "2018-04-23 20:49:32"
  		}, {
  			"name": "vpn申请", // 流程名称
  			"creator": "admin", // 创建人
  			"description": "vpn权限申请", // 描述
  			"gmt_created": "2018-05-06 12:32:36" //创建时间
  		}]
  	},
  	"msg": ""
  }
  ```

## 创建流程字段（获取工作流初始状态）

- /api/v1/service/init_state/${id}/

- get

  ```javascript
  {
  	"msg": "",
  	"code": 0,
  	"data": {
  		"order_id": 0,
  		"workflow_id": 1,
  		"name": "新建中",
  		"participant_type_id": 1,
  		"distribute_type_id": 1,
  		"participant": "wangfei",
  		"is_hidden": false,
  		"type_id": 1,
  		"gmt_created": "2018-04-23 20:53:33",
  		"id": 1,
  		"transition": [{   // 改流程可操作的按钮
  			"transition_id": 1,
  			"transition_name": "提交"
  		}, {
  			"transition_id": 2,
  			"transition_name": "保存"
  		}],
  		"sub_workflow_id": 0, // 子流程id
  		"creator": "admin",
  		"label": {},
  		"field_list": [{  // 需要的字段列表
  			"order_id": 20, // 展示的顺序
  			"field_key": "title",//改字段的key
  			"field_attribute": 2, //字段属性（1：只读，2:必填，3:可选）
  			"value": null, // 字段值
  			"name": "标题", // 对应的lable
  			"field_type_id": 5 // 字段类型id 
        `
  				1: 常规流转
          2: 定时器流转(在v0.2以后版本中支持)自定义字段类型
          5: 字符串
          10: 整形
          15: 浮点型
          20: 布尔类型
          25: 日期类型
          30: 日期时间类型
          35: 单选框radio
          40: 多选框checkbox
          45: 下拉列表
          50: 多选的下拉列表
          55: 文本域
          60: 用户名(需要调用方系统自行处理用户列表，loonflow只保存用户名)
          70: 多选用户名(需要调用方系统自行处理用户列表，loonflow只保存用户名，多人的情况使用逗号隔开)
          80: 附件，多个附件使用逗号隔开。调用方自己实现上传功能，loonflow只保存文件路径
        `
        
  		}, {
  			"order_id": 35,
  			"field_key": "leave_proxy",
  			"field_attribute": 2,
  			"field_type_id": 60, // 60 可选择的
  			"field_value": null,
  			"field_name": "代理人",
  			"field_choice": {} // 可选择的（人物可能需要自己填充 「‘id’：name」）
  		}, {
  			"order_id": 25,
  			"field_key": "leave_end",
  			"field_attribute": 2,
  			"field_type_id": 30,
  			"field_value": null,
  			"field_name": "结束时间",
  			"field_choice": {}
  		}, {
  			"order_id": 20,
  			"field_key": "leave_start",
  			"field_attribute": 2,
  			"field_type_id": 30,
  			"field_value": null,
  			"field_name": "开始时间",
  			"field_choice": {}
  		}, {
  			"order_id": 40,
  			"field_key": "leave_type",
  			"field_attribute": 2,
  			"field_type_id": 40,
  			"field_value": null,
  			"field_name": "请假类型",
  			"field_choice": {
  				"1": "年假",
  				"2": "调休",
  				"3": "病假",
  				"4": "婚假"
  			}
  		}, {
  			"order_id": 45,
  			"field_key": "leave_reason",
  			"field_attribute": 2,
  			"field_type_id": 55,
  			"field_value": null,
  			"field_name": "请假原因及相关附件",
  			"field_choice": {}
  		}, {
  			"order_id": 30,
  			"field_key": "leave_days",
  			"field_attribute": 2,
  			"field_type_id": 5,
  			"field_value": null,
  			"field_name": "请假天数(0.5的倍数)",
  			"field_choice": {}
  		}]
  	}
  }
  ```

  

