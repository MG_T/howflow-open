# howflow-open

## 介绍

- 本项目旨在替代钉钉自带的“审批”应用。
- 本项目基于[loonflow](https://github.com/blackholll/loonflow)引擎。


## 技术栈

- loonflow 1.0
- django REST Framework
- Vue 2.x
- 钉钉（移动端H5应用）
- 腾讯云COS


## 安装教程

计划中


## 使用说明

对最终用户而言，使用体验几乎与钉钉自带审批一致


## 已完成 & 计划中

- [x] 基本功能内侧完成
- [ ] 安装教程


## 鸣谢

- 感谢 loonflow 项目维护者 blackholll 的杰出贡献。


## 支持开源
开源不易，需要您的支持，支持方式：
### 1. star 本项目
### 2. 贡献代码
1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
### 3. 提bug或需求
- https://gitee.com/shihow/howflow-open/issues
- 提issue前请先搜索，避免重复
### 4. 仅仅是使用本项目
### 5. 捐赠
- 1分钱也是支持
- ![捐赠](https://images.gitee.com/uploads/images/2020/0503/125753_287d1c22_1333971.png "屏幕截图.png")


## 效果展示
[演示视频](https://gitee.com/shihow/howflow-open/attach_files)
![效果展示1](https://images.gitee.com/uploads/images/2020/0526/171735_cea92cbd_1333971.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0526/171821_8bff6809_1333971.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0526/171828_b8a54e38_1333971.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0526/171836_3e5a5a43_1333971.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0526/171843_369776d6_1333971.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0526/171850_c7afa538_1333971.png "屏幕截图.png")
